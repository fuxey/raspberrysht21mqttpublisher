/*
 * mqttpublish.h
 *
 *  Created on: 28.01.2018
 *      Author: fuchs
 */

#ifndef MQTTPUBLISH_H_
#define MQTTPUBLISH_H_





class mqttpublish {
private:
	char ClientId[20];
	char address[100];
	char QOS;
public:
mqttpublish();
void mqtt_setQOS(char qos);
void mqtt_setaddress(char *adrstr);
void mqtt_setClientId(char *clientid);
int mqtt_publishTopic(char *topic, char *payload);
};


#endif /* MQTTPUBLISH_H_ */
