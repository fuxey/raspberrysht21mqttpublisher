#include "sht21.h"
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <unistd.h>
#include "logfile.h"


SHT21::SHT21() {
    int address = 0x40;
      /* Read from I2C and print temperature */
     int fdi2c = wiringPiI2CSetup(address);
     if(fdi2c < 0){
    	 Debug::CRITICALMESSAGE("cannot create i2c sht21");
     }else{
    	 Debug::WARNING("File Descriptor of SHT21 i2C %d",fdi2c);
     }
    cnt=0;
}



double SHT21::getHumidity() {
	this->measureHumidity();
	return humidity;
}

double SHT21::getTemperature() {
	this->measureTemperature();
    return temperature;
}

// Get temperature
bool SHT21::measureTemperature() {

    bool error = false;
    unsigned char buf [5];
    unsigned char data[3];
    data [0] = HTU21D_TEMP ;
    if (write (fdi2c, data, 1) != 1) {

    	Debug::WARNING("measure Temperature cannot write on i2c");
    	return false;

    }
    delay(260);
    // i think here is the problem
    int byte = read(fdi2c, buf, 3);
    if(byte != 4){
        error = true;
    }
    unsigned int temp = (buf [0] << 8 | buf [1]) & 0xFFFC;
    // Convert sensor reading into temperature.
    // See page 14 of the datasheet
    Debug::DEBUGMESSAGE("SHT21 temp: %x %x %x %x %x",temp,buf[0],buf[1],buf[2],buf[3]);
	uint32_t temperature_ = temp *17572;
	temperature_ >>=16;
	this->temperature = temperature_ -= 4685;
    return error;
}

// Get humidity
bool SHT21::measureHumidity() {

    bool error = false;
    unsigned char buf [5];
    unsigned char data[3];
    data [0] = HTU21D_HUMID ;
    if (write (fdi2c, data, 1) != 1) {
    	Debug::WARNING("measure Humidity cannot write on i2c");
    	return false;
    }

    delay(70);
    int bytes = read(fdi2c, buf, 3);
    if(bytes != 4){
        error = true;
    }
    unsigned int humid = (buf [0] << 8 | buf [1]) & 0xFFFC;
    Debug::DEBUGMESSAGE("SHT21 humi:  %x %x %x %x %x",humid,buf[0],buf[1],buf[2],buf[3]);
    // Convert sensor reading into humidity.
    // See page 15 of the datasheet
	uint32_t humidity_ = humid *= 12500;
	humidity_ >>= 16;
	this->humidity = humidity_ -= 600;

    return error;
}
