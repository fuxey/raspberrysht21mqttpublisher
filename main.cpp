
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <getopt.h>
#include <time.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <sys/file.h>
#include <errno.h>
#include "sysstats.h"
#include "logfile.h"
#include "mqttpublish.h"
#include  "lm75.h"
#include "sht21.h"
#include <wiringPi.h>
#include <lcd.h>
#include <unistd.h>
#include <string.h> /* for strncpy */
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <wiringPi.h>
#include <softPwm.h>
#include <wiringPi.h>
#include <lcd.h>


#define BACKLIGHT 29


class LcdShield {
int lcdHandle;
int softpwm;
#define LCDWIDTH 16
public:
LcdShield() {
    lcdHandle = lcdInit (2, 16, 4, 3,4, 5,6,25,2,0,0,0,0) ;

    if (lcdHandle < 0)
    {
        Debug::CRITICALMESSAGE( "Cannot Open LCD!") ;

    }else{
        Debug::CRITICALMESSAGE( "File Descriptor of LCD Display is %d", lcdHandle);
    }

    if(softPwmCreate(BACKLIGHT,1,100) < 0){
      Debug::CRITICALMESSAGE("Cannot create Softpwm!");

    }
    softpwm = BACKLIGHT;

}

void printwifiIP(int row, int col) {

    int fd;
    struct ifreq ifr;


    fd = socket(AF_INET, SOCK_DGRAM, 0);

    /* I want to get an IPv4 IP address */
    ifr.ifr_addr.sa_family = AF_INET;

    /* I want IP address attached to "eth0" */
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);

    ioctl(fd, SIOCGIFADDR, &ifr);

    close(fd);
    char message[20];
    memset(message,0,sizeof(message));
    /* display result */
    sprintf(message,"Ip:%s",inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
    Debug::DEBUGMESSAGE("IP %s",message);
    lcdPosition (lcdHandle, row, col) ;
    lcdPuts     (lcdHandle, message) ;


}

void turnOnBacklight() {
	softPwmWrite(BACKLIGHT,100);
}
void turnOffBacklight() {
	softPwmWrite(BACKLIGHT,0);
}
void printString(char * str,int row, int col) {
    lcdPosition (lcdHandle, row, col) ;
    lcdPuts     (lcdHandle, str) ;

}

};

class Getter {

private:

	mqttpublish mqtt;
	sysstats sys;

	SHT21 tempsht21;
	LM75 tempLM75;

	char* createNetworkSpeedString(char *stringBuffer) {
		long long Bytes = sys.getBytesPerSecond();
		long long kByte = 0;
		long long mByte = 0;
		memset(stringBuffer,0,20);
		if(Bytes > 1000) {
			kByte = Bytes / 1000;
			Bytes %= 1000;
		}
		if(kByte > 1000) {
			mByte = kByte / 1000;
			kByte %= 1000;
		}
		sprintf(stringBuffer,"%lldByte/s    ",Bytes);
		if(mByte > 0) {
			sprintf(stringBuffer,"%lld.%1lldmByte/s    ",mByte,kByte/100);
			Bytes = 0;
			kByte = 0;
			mByte = 0;
		}

		if(kByte > 0) {
			sprintf(stringBuffer,"%lld.%1lldkByte/s    ",kByte,Bytes/100);
			Bytes = 0;
			kByte = 0;
			mByte = 0;
		}

		if(Bytes >  0) {
			sprintf(stringBuffer,"%lldByte/s    ",Bytes);
			Bytes = 0;
			kByte = 0;
			mByte = 0;
		}
		return stringBuffer;
	}


//	static void mqtt_publishTempAndHum(int temp, int humi) {
//
//		char payload[200];
//		sprintf(payload,"{Temperature:%d.%d, Humidity:%d.%d}\n",temp/100,temp%100,humi/100,humi%100);
//		DEBUGMESSAGE("publish:%s",payload);
//		mqtt_publishTopic("NAS/HUMITEMP",payload);
//	}



public:
	Getter() {
//		mqtt = new mqttpublish;
//		sys = new sysstats;
		Debug::DEBUGMESSAGE("start SW");

		sys.initNetworkMonitor();

		Debug::DEBUGMESSAGE("set Mqtt Settings");
		mqtt.mqtt_setClientId("RASPI");
		mqtt.mqtt_setQOS(1);
		mqtt.mqtt_setaddress("tcp://192.168.1.2:1883");
		sys.initcpuUsage();

	}
	void mqtt_publishSysStat() {

		char payload[120];
		char stringbuffer[20];
		time_t ltime; /* calendar time */
		ltime=time(NULL); /* get current cal time */
		struct tm *rtime = localtime(&ltime);

		sprintf(payload,"{CurrentTime:%02d:%02d:%02d,CPUUsage: CPU:%3.1lf%%,networkUsage:%s}",
				rtime->tm_hour,rtime->tm_min,rtime->tm_sec,sys.getCurrentCpuUsage(),createNetworkSpeedString(stringbuffer));
		mqtt.mqtt_publishTopic("RASPI/SYSstat",payload);
	}

	void mqtt_publishTempAndHum() {

		int temp = (int)tempsht21.getTemperature();
		int humi = (int)tempsht21.getHumidity();

		temp *= 100;
		humi *= 100;
		char payload[200];
		sprintf(payload,"{Temperature:%d.%d, Humidity:%d.%d}\n",temp/100,temp%100,humi/100,humi%100);
		Debug::DEBUGMESSAGE("publish:%s",payload);
		mqtt.mqtt_publishTopic("RASPI/HUMITEMPSHT21",payload);
	}

	void mqtt_publishTempLM75() {
		int temp = (int)tempLM75.getTemperature();

		temp *= 100;
		char payload[200];
		sprintf(payload,"{Temperature:%d.%d}\n",temp/100,temp%100);
		Debug::DEBUGMESSAGE("publish:%s",payload);
		mqtt.mqtt_publishTopic("RASPI/TEMPLM75",payload);
	}

};


//
//fd = socket(AF_INET, SOCK_DGRAM, 0);
//
///* I want to get an IPv4 IP address */
//ifr.ifr_addr.sa_family = AF_INET;
//
///* I want IP address attached to "eth0" */
//strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);
//
//ioctl(fd, SIOCGIFADDR, &ifr);
//
//close(fd);
//
///* display result */
//printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
//  sprintf(message,"Server Ip:%s TestRaspberry",inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));


int main(int argc, char *argv[]) {

	(void)argc;
	(void)argv;

    if(wiringPiSetup () <0){
       Debug::CRITICALMESSAGE("Unable to Setup wiringPi");
       return -1;
    }

   if(wiringPiSetupSys() < 0){
	   Debug::CRITICALMESSAGE("Unable to Setup wiringPISYS");
    }




//	Debug::setVerbosityLevel(Debug::vrlvl_warning);
	Getter mqttpublish;
	LcdShield lcd;




	lcd.printwifiIP(0,0);

	lcd.turnOffBacklight();


    while(1) {

    	usleep(200000);
    	mqttpublish.mqtt_publishSysStat();
    	usleep(200000);
    	mqttpublish.mqtt_publishTempAndHum();
    	usleep(200000);
    	mqttpublish.mqtt_publishTempLM75();
    	usleep(5000000);
	}
    return 0;
}
