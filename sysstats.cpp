/*
 * sysstats.c
 *
 *  Created on: 02.01.2018
 *      Author: fuchs
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <getopt.h>
#include <time.h>
#include "logfile.h"
#include <sys/statvfs.h>
#include <sys/vfs.h>
#include <sys/types.h>
#include <pwd.h>
#include "sysstats.h"

sysstats::sysstats() {

}

long sysstats::getFreeDiskSpace(const char* absoluteFilePath) {
   struct  statvfs buf;

   if (! statvfs(absoluteFilePath, &buf)) {

      unsigned long blksize, blocks, freeblks, disk_size, used, free;

      printf("blksize :  %ld\n",buf.f_bsize);
      printf("blocks :  %ld\n",buf.f_blocks);
      printf("bfree :  %ld\n",buf.f_bfree);
      printf("bavail: %ld\n",buf.f_bavail);
      printf("f_frsize:%ld\n",buf.f_frsize);


      blksize = buf.f_bsize;
      blocks = buf.f_blocks;
      freeblks = buf.f_bfree;



      disk_size = blocks*blksize;
      free = freeblks*blksize;
      used = disk_size - free;

      printf("disk %s disksize: %ld free %ld used %ld\n",absoluteFilePath,disk_size,free,used);
      return free;
   }
   else {
      return -1;
   }
   return -1;
}

long long sysstats::userAvailableFreeSpace() {
    struct statvfs stat;
    struct passwd *pw = getpwuid(getuid());
    if ( NULL != pw && 0 == statvfs(pw->pw_dir, &stat) )
    {
    	printf("path %s\n",pw->pw_dir);
    	long long freeBytes = (uint64_t)stat.f_bavail * stat.f_frsize;
        return freeBytes;
    }
    return 0ULL;
}

void sysstats::initNetworkMonitor() {
	FILE *file = fopen("/proc/net/dev","r");
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    before = clock();

    while ((read = getline(&line, &len, file)) != -1) {
 	   sscanf(line,"  eth0: %llu",&bytestransmitted);
    }
	fclose(file);
}

long long sysstats::getBytesPerSecond() {
	unsigned long long oldbytestransmitted = bytestransmitted;
	clock_t oldclock;
	memcpy(&oldclock,&before,sizeof(clock_t));

	FILE *file = fopen("/proc/net/dev","r");
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, file)) != -1) {
	   sscanf(line,"  eth0: %llu",&bytestransmitted);
    }

	fclose(file);
	before = clock();
	clock_t diff = clock() - oldclock;
	int msec = diff *1000 / CLOCKS_PER_SEC;
	if(msec <=  0) {
		msec = 1;
	}
	long long Bytes = bytestransmitted - oldbytestransmitted;
	Bytes *= 1000;
	Bytes /= msec;
	Bytes /= 1000;

	return Bytes;  // bytes per second
}

void sysstats::initcpuUsage() {
    FILE* file = fopen("/proc/stat", "r");
    fscanf(file, "cpu %llu %llu %llu %llu", &lastTotalUser, &lastTotalUserLow,
        &lastTotalSys, &lastTotalIdle);
    fclose(file);
}

double sysstats::getCurrentCpuUsage() {
    double percent;
    FILE* file;
    unsigned long long totalUser, totalUserLow, totalSys, totalIdle, total;

    file = fopen("/proc/stat", "r");
    fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow,
        &totalSys, &totalIdle);
    fclose(file);

    if (totalUser < lastTotalUser || totalUserLow < lastTotalUserLow ||
        totalSys < lastTotalSys || totalIdle < lastTotalIdle){
        //Overflow detection. Just skip this value.
        percent = -1.0;
    }
    else{
        total = (totalUser - lastTotalUser) + (totalUserLow - lastTotalUserLow) +
            (totalSys - lastTotalSys);
        percent = total;
        total += (totalIdle - lastTotalIdle);
        percent /= total;
        percent *= 100;
    }

    lastTotalUser = totalUser;
    lastTotalUserLow = totalUserLow;
    lastTotalSys = totalSys;
    lastTotalIdle = totalIdle;

    return percent;
}

long sysstats::get_mem_total (void) {
   FILE *fp;
   char buffer[4000];
   size_t bytes_read;
   char *match;
   long mem_tot;
   if((fp = fopen("/proc/meminfo", "r")) == NULL) {
      perror("fopen()");
      exit(EXIT_FAILURE);
   }
   bytes_read = fread (buffer, 1, sizeof (buffer), fp);
   fclose (fp);
   if (bytes_read == 0 || bytes_read == sizeof (buffer))
      return 0;
   buffer[bytes_read] = '\0';
   match = strstr (buffer, "MemTotal");
   if (match == NULL)
      return 0;
   sscanf (match, "MemTotal: %ld", &mem_tot);
   return (mem_tot/1024); /* 1MB = 1024KB */
}

void sysstats::error(const char *msg) {
    perror(msg);
    exit(1);
}

