#ifndef SHT21_H
#define SHT21_H


#include <stdint.h>
#define	ERROR_SHT21_I2C				1
#define	ERROR_SHT21_CRC_TEMP		2
#define	ERROR_SHT21_CRC_HUMIDITY	4

#define   HTU21D_I2C_ADDR 0x40

#define   HTU21D_TEMP     0xF3
#define   HTU21D_HUMID    0xF5

class SHT21
{

public:
    SHT21();
    double getTemperature();
    double getHumidity();
private:
    int fdi2c;
    void DelayMs(uint32_t ms);
    double temperature;
    double humidity;
    bool measureTemperature();
    bool measureHumidity();
    int cnt;
};



#endif // SHT21_H
