#ifndef LM75_H
#define LM75_H


class LM75 {
public:
    LM75();
    float getTemperature();
    void setTemperature(float value);

private:
    int fd;
    float Temperature;
    void MeasureTemperature();
};

#endif // LM75_H
