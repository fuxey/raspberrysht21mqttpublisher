EXEC = raspberrypisht21mqttpublisher


SOURCES += \
	sysstats.cpp \
	mqttpublish.cpp \
	logfile.cpp \
	lm75.cpp \
	sht21.cpp \
	main.cpp
	

#OBJECTS = $(SOURCES:.c=.o)
OBJECTS += $(SOURCES:.cpp=.o)

CC ?= g++
CXX = g++

doxygen ?=doxygen

CC_FLAGS ?= -Wall -Wextra -pedantic -g -std=gnu++11  -D_DEFAULT_SOURCE
CXXFLAGS = $CC_FLAGS
LD_FLAGS ?= -lm -pthread -lpaho-mqtt3c -lpaho-mqtt3a -lwiringPi -lwiringPiDev
CC_DEBUG ?= -g -gdwarf -ggdb 

INCPATH = -I./


all: $(EXEC)

debug: $(EXEC) $(CC_FLAGS) += -g -gdwarf -ggdb


$(EXEC): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXEC) $(LD_FLAGS)


#%.o: %.c
#	$(CC) -c $(CC_FLAGS) $< -o $@

%.o: %.cpp
	$(CC) -c $(CC_FLAGS) $< -o $@

clean:
	rm -f $(EXEC) $(OBJECTS)

doxy:
	$(doxygen) $(SOURCES)
	
execute:
	rm -f $(EXEC) $(OBJECTS)
	$(EXEC)
	sudo ./NASLCDclient