#ifndef __LOGFILE__
#define __LOGFILE__


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <time.h>
#include <poll.h>




class Debug {
private:
public:
enum verbositylevels {
    vrlvl_debug =0x01,
    vrlvl_info,
    vrlvl_important,
    vrlvl_warning,
    vrlvl_critical,
    vrlvl_fatal
};

static void setVerbosityLevel(int lvl);

static void INFOMESSAGE(char* format,...);

static void DEBUGMESSAGE(char* format,...);

static void IMPORTANTMESSAGE(char* format,...);

static void WARNING(char* format,...);

static void CRITICALMESSAGE(char* format,...);

static void FATALMESSAGE(char* format,...);

static void writeverbositylog(int, char* format,va_list );

static void writelog(char* format,...);

};

#endif
