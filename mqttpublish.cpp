/*
 * mqttpublish.c
 *
 *  Created on: 28.01.2018
 *      Author: fuchs
 */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <MQTTClient.h>
#include "logfile.h"
#include "mqttpublish.h"

#define QOS__         1
#define TIMEOUT     10000L




mqttpublish::mqttpublish() {

}
void mqttpublish::mqtt_setQOS(char qos) {
	QOS = qos;
}

void mqttpublish::mqtt_setaddress(char *adrstr) {
	strcpy(address,adrstr);
}

void mqttpublish::mqtt_setClientId(char *clientid) {
	strcpy(ClientId,clientid);
}

int mqttpublish::mqtt_publishTopic(char *topic, char *payload) {

    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    MQTTClient_message pubmsg = MQTTClient_message_initializer;
    MQTTClient_deliveryToken token;
    int rc;

    Debug::DEBUGMESSAGE("create mqtt client: adr %s, client %s,\n",address,ClientId);
    MQTTClient_create(&client, address, ClientId,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;

    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS) {
    	Debug::CRITICALMESSAGE("Failed to connect, return code %d\n", rc);
        exit(EXIT_FAILURE);
    }

    pubmsg.payload = payload;
    pubmsg.payloadlen = strlen(payload);


    pubmsg.qos = QOS;
    pubmsg.retained = 0;

    Debug::DEBUGMESSAGE("mqtt publish topic: %s, payload %s \n",topic,payload);
    MQTTClient_publishMessage(client, topic, &pubmsg, &token);
    rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
    Debug::DEBUGMESSAGE("Message with delivery token %d delivered\n", token);
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return rc;
}

