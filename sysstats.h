/*
 * sysstats.h
 *
 *  Created on: 02.01.2018
 *      Author: fuchs
 */

#ifndef SYSSTATS_H_
#define SYSSTATS_H_
#include <time.h>

class sysstats {

public:

	sysstats();
	void initNetworkMonitor();
	long long getBytesPerSecond();

	long long userAvailableFreeSpace();
	void initcpuUsage();
	double getCurrentCpuUsage();

	long get_mem_total (void);
	long getFreeDiskSpace(const char* absoluteFilePath);
private:
	 unsigned long long lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;
	 unsigned long long bytestransmitted ;
	 clock_t before;
	 void error(const char *msg);
};

#endif /* SYSSTATS_H_ */
