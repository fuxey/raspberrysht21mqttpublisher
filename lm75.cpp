#include "lm75.h"
#include <wiringPi.h>
#include <wiringPiI2C.h>


float LM75::getTemperature() {
	MeasureTemperature();
    return Temperature;
}

void LM75::setTemperature(float value)
{
    Temperature = value;
}
LM75::LM75() {

    int address = 0x48;
    /* Read from I2C and print temperature */
    fd= wiringPiI2CSetup(address);
   if(fd < 0){
//      qCritical() << "Cannot Open  LM75 I2C";

   }else{
//       qWarning() << "File Descriptor of LM75 i2C" << fd;
   }


}

void LM75::MeasureTemperature() {
    int raw = wiringPiI2CReadReg16(fd, 0x00);
    raw = ((raw << 8) & 0xFF00) + (raw >> 8);
//    DEBUGMSG() << "Read Temperature from LM75" << (float)((raw / 32.0) / 8.0);
    this->setTemperature((float)((raw / 32.0) / 8.0));
}
